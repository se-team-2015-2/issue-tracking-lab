﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    public static class MatrixHandler
    {
		public static int GetCountPositiveElementsSecondaryDiagonal(int[,] matrix)
        {
			int count = 0;
			var size = matrix.GetLength (0) - 1;
			for (var i = size; i >= 0; i--) 
			{
				if (matrix [i, size - i] > 0)
				{
					count++;
				}
			}
			return count;
        }

		public static void SetZeroNegativeElementsBelowMainDiagonal(int[,] matrix)
        {
			var size = matrix.GetLength (0);
			for (var i = 0; i <= size; i++) 
			{
				for (var j = i + 1; j < size; j++) 
				{
					if (matrix [j, i] < 0) 
					{
						matrix [j, i] = 0;
					}
				}
			}
        }
    }
}
