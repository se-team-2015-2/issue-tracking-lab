﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    public class Program
    {
        static void Main(string[] args)
        {
            const int Size = 7;
            int[,] matrix = new int[Size, Size];

            FillMatrix(matrix);
            WriteMatrix(matrix);
            Console.WriteLine();

            var count = MatrixHandler.GetCountPositiveElementsSecondaryDiagonal(matrix);
            Console.WriteLine("Количество положительных элементов на побочной диагонали: {0}.\n", count);

            MatrixHandler.SetZeroNegativeElementsBelowMainDiagonal(matrix);
            Console.WriteLine("Матрица после замены отрицательных элементов ниже главной диагонали на 0\n");
            WriteMatrix(matrix);

            Console.ReadKey();
        }

        public static void FillMatrix(int[,] matrix)
        {
            const int MaxValue = 22;
            const int MinValue = -20;

            Random rand = new Random();

            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(MinValue, MaxValue + 1); 
                }
            }
        }

        public static void WriteMatrix(int[,] matrix)
        {
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("{0,6}", matrix[i, j]);
                }

                Console.WriteLine();
            }
        }
    }
}
